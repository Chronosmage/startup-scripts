#Alexandra's Fixes for Eternal
#Made for Modrealms

#Import the necessary recipe providers
import mods.tconstruct.Alloy;
#Announce the script's loading
print("--- loading Alexa-Fixes.zs ---");

#Removing broken recipes
print("[Alexa-Fixes] Removing broken or conflicting recipes...");

#Conflict
recipes.remove(<iceandfire:blindfold>);
recipes.remove(<improvedbackpacks:item.bound_leather>);

#Conflicts with minecraft:stick
recipes.remove(<mod_lavacow:mossy_stick>);

#Conflicts with extrautils2:compressedcobblestone and is also useless at this time
recipes.remove(<from_the_depths:item_totem_of_summoning>);

#Remove the alloying recipe for Fluxed Electrum since it's useless
mods.tconstruct.Alloy.removeRecipe(<liquid:electrumflux>);

#Adding replacement recipes
print("[Alexa-Fixes] Adding replacement recipes...");

recipes.addShaped(<improvedbackapacks:bound_leather>, [[null, null, null],[<ore:string>, <ore:leather>, <ore:string>],[null, null, null]]);

recipes.addShaped(<iceandfire:blindfold>, [[<ore:string>, <ore:leather>, <ore:string>],[null, null, null],[null, null, null]]);

recipes.addShaped(<mod_lavacow:mossy_stick> * 16, [[null, null, null], [null, <ore:logWood>, null], [<ore:logWood>, null, null]]);

print("--- Alexa-Fixes.zs initialized ---");