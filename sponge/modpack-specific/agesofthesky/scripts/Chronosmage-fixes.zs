#Chronosmage-Fixes
#Made for modrealms

import mods.extendedcrafting.CompressionCrafting;

#Announce scripts loading
print("--- Loading Chronosmage-Fixes.zs ---");

#Adding Explosive recipe via crafting
print("--- Adding Explosive recipe to Table ---");

recipes.addShapeless(<pneumaticcraft:ingot_iron_compressed> * 1, [<minecraft:iron_ingot:0> * 1, <minecraft:tnt>]);
recipes.addShapeless(<pneumaticcraft:ingot_iron_compressed> * 1, [<minecraft:iron_ingot:0> * 1, <appliedenergistics2:tiny_tnt:0>]);

#Adding Custom twilight recipes
print("--- Adding Custon Twilight Recipes ---");

recipes.addShaped(<twilightforest:fire_jet:3> * 1, [[<minecraft:stone>, <minecraft:hopper>, <minecraft:stone>], [<minecraft:stone>, <minecraft:gunpowder>, <minecraft:stone>], [<minecraft:stone>, <minecraft:stone>, <minecraft:stone>] ]);

#remove Compression Crafting recipe

print("--- Removing Compression singularity ---");

mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity_custom:7>);

#Add Compression crafting Recipe

mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity_custom:7>, <extendedcrafting:singularity_custom:2>, 2000, <extendedcrafting:material:11>, 5000000);

print("--- Chronosmage-Fixes.zs Applied all changes ---");