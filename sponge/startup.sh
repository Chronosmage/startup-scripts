#!/bin/bash
PLUGIN_BRANCH=$1
HOST=$2
JARFILE=$3
JMX_PORT=$4

if [ -d "world/betterquesting" ] ;then
    questBackupDirName="$(date -I)_$(awk "BEGIN {print $(ls ./questProgressBackups/$(date -I)* | wc -l)+1;exit}")"
    mkdir -p "./questProgressBackups/$questBackupDirName"
    cp "./world/betterquesting/QuestProgress.json" "./questProgressBackups/$questBackupDirName/"
fi

mkdir -p plugins
curl -s "https://gitlab.com/api/v4/projects/14547790/jobs/artifacts/${PLUGIN_BRANCH}/raw/Sponge/build/libs/Punishments-sponge.jar?job=build" -L --output plugins/punishments-sponge.jar
curl -s "https://gitlab.com/api/v4/projects/14547656/jobs/artifacts/${PLUGIN_BRANCH}/raw/Sponge/build/libs/Chat-sponge.jar?job=build" -L --output plugins/chat-sponge.jar
curl -s "https://gitlab.com/api/v4/projects/13686960/jobs/artifacts/${PLUGIN_BRANCH}/raw/Sponge/build/libs/ConnectionBridge-sponge.jar?job=build" -L --output plugins/bridge-sponge.jar
curl -s "https://gitlab.com/api/v4/projects/14547906/jobs/artifacts/${PLUGIN_BRANCH}/raw/Sponge/build/libs/Rewards-sponge.jar?job=build" -L --output plugins/rewards-sponge.jar
curl -s "https://gitlab.com/api/v4/projects/14547729/jobs/artifacts/${PLUGIN_BRANCH}/raw/Sponge/build/libs/Achievements-sponge.jar?job=build" -L --output plugins/achievements-sponge.jar
curl -s "https://gitlab.com/api/v4/projects/14548277/jobs/artifacts/${PLUGIN_BRANCH}/raw/Sponge/build/libs/Chunkloading-sponge.jar?job=build" -L --output plugins/chunkloader-sponge.jar
curl -s "https://gitlab.com/api/v4/projects/14547898/jobs/artifacts/${PLUGIN_BRANCH}/raw/Sponge/build/libs/Worlds-sponge.jar?job=build" -L --output plugins/worlds-sponge.jar
curl -s "https://gitlab.com/api/v4/projects/14547914/jobs/artifacts/${PLUGIN_BRANCH}/raw/Sponge/build/libs/ShopMenu-sponge.jar?job=build" -L --output plugins/shopmenu-sponge.jar
curl -s "https://gitlab.com/api/v4/projects/14547749/jobs/artifacts/${PLUGIN_BRANCH}/raw/Sponge/build/libs/Titles-sponge.jar?job=build" -L --output plugins/titles-sponge.jar
curl -s "https://gitlab.com/api/v4/projects/14547955/jobs/artifacts/${PLUGIN_BRANCH}/raw/Sponge/build/libs/commands-sponge.jar?job=build" -L --output plugins/commands-sponge.jar
curl -s "https://gitlab.com/api/v4/projects/14940205/jobs/artifacts/${PLUGIN_BRANCH}/raw/Sponge/build/libs/Restrictions-sponge.jar?job=build" -L --output plugins/restrictions-sponge.jar
curl -s "https://gitlab.com/api/v4/projects/14986737/jobs/artifacts/${PLUGIN_BRANCH}/raw/Sponge/build/libs/Backups-sponge.jar?job=build" -L --output plugins/backups-sponge.jar
curl -s "https://gitlab.com/api/v4/projects/14547834/jobs/artifacts/${PLUGIN_BRANCH}/raw/Sponge/build/libs/Announcements-sponge.jar?job=build" -L --output plugins/announcements-sponge.jar

java -Xmx8144M -Xms8144M -Xmn128M -XX:+DisableExplicitGC -XX:+UseConcMarkSweepGC -XX:+UseParNewGC -XX:+UseNUMA -XX:+CMSParallelRemarkEnabled -XX:MaxTenuringThreshold=15 -XX:MaxGCPauseMillis=30 -XX:GCPauseIntervalMillis=150 -XX:+UseAdaptiveGCBoundary -XX:-UseGCOverheadLimit -XX:+UseBiasedLocking -XX:SurvivorRatio=8 -XX:TargetSurvivorRatio=90 -XX:MaxTenuringThreshold=15 -Dfml.ignorePatchDiscrepancies=true -Dfml.ignoreInvalidMinecraftCertificates=true -XX:+UseFastAccessorMethods -XX:+UseCompressedOops -XX:+OptimizeStringConcat -XX:+AggressiveOpts -XX:ReservedCodeCacheSize=2048m -XX:+UseCodeCacheFlushing -XX:SoftRefLRUPolicyMSPerMB=10000 -XX:ParallelGCThreads=10 -Dfml.readTimeout=120 -XX:HeapDumpPath=MojangTricksIntelDriversForPerformance_javaw.exe_minecraft.exe.heapdump -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=${JMX_PORT} -Dcom.sun.management.jmxremote.rmi.port=${JMX_PORT} -Dcom.sun.management.jmxremote.local.only=false -Dcom.sun.management.jmxremote.authenticate=true -Djava.rmi.server.hostname=94.130.35.179 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.password.file=./jmxremote.password -Dcom.sun.management.jmxremote.access.file=./jmxremote.access -XX:+UnlockCommercialFeatures -XX:+FlightRecorder  -Dfml.doNotBackup=true -Dhost=${HOST} -jar ${JARFILE}