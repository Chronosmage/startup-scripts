curl -s "https://gitlab.com/api/v4/projects/14547790/jobs/artifacts/master/raw/Sponge/build/libs/Punishments-sponge.jar?job=build" -L --output mods/punishments-sponge.jar

curl -s "https://gitlab.com/api/v4/projects/14547656/jobs/artifacts/master/raw/Sponge/build/libs/Chat-sponge.jar?job=build" -L --output mods/chat-sponge.jar

curl -s "https://gitlab.com/api/v4/projects/13686960/jobs/artifacts/master/raw/SpongeHub/build/libs/ConnectionBridge-hub.jar?job=build" -L --output mods/bridge-hub.jar

# curl -s "https://gitlab.com/api/v4/projects/14547906/jobs/artifacts/master/raw/Sponge/build/libs/rewards-sponge.jar?job=build" -L --output mods/rewards-sponge.jar

curl -s "https://gitlab.com/api/v4/projects/14547729/jobs/artifacts/master/raw/Sponge/build/libs/Achievements-sponge.jar?job=build" -L --output mods/achievements-sponge.jar

curl -s "https://gitlab.com/api/v4/projects/14547914/jobs/artifacts/master/raw/Sponge/build/libs/ShopMenu-sponge.jar?job=build" -L --output mods/shopmenu-sponge.jar

curl -s "https://gitlab.com/api/v4/projects/14547749/jobs/artifacts/master/raw/Sponge/build/libs/Titles-sponge.jar?job=build" -L --output mods/titles-sponge.jar

curl -s "https://gitlab.com/api/v4/projects/14547955/jobs/artifacts/master/raw/Sponge/build/libs/commands-sponge.jar?job=build" -L --output mods/commands-sponge.jar

# curl -s "https://gitlab.com/api/v4/projects/14547834/jobs/artifacts/master/raw/Sponge/build/libs/announcements-sponge.jar?job=build" -L --output mods/announcements-sponge.jar

echo 'Successfully deployed the latest version of all plugins!'

echo "Starting Server!"

echo "Host: $1"

echo "Startup Jar: $2"

java -XX:+UseG1GC -Xmx3072M -Xms3072M -Dsun.rmi.dgc.server.gcInterval=2147483646 -XX:+UnlockExperimentalVMOptions -XX:G1NewSizePercent=20 -XX:G1ReservePercent=20 -XX:MaxGCPauseMillis=50 -XX:G1HeapRegionSize=32M -Dfml.doNotBackup=true -Dfml.readTimeout=120 -Dhost=$1 -jar $2