#!/bin/bash
# <CHANGE> Installation Script
# Arguments: $1=Project ID $2=File ID $3=Server Jarfile $4=Modpack Tag
echo "<CHANGE> Installation Script Started!";

# Setting Variables
PROJECT_ID=$1
echo -e "Project ID: $PROJECT_ID"
FILE_ID=$2
echo -e "File ID: $FILE_ID"
SERVER_JARFILE=$3
echo -e "Server Jarfile: $SERVER_JARFILE"
MODPACK_TAG=$4
echo -e "Modpack Tag: $MODPACK_TAG"

#Before we do anything important, make a backup in gzip-compressed tape archive format
#Done from outside of the server directory to prevent tar from complaining about "." having changed as it was read
echo "Creating backup..."
cd /tmp
tar --restrict --directory='/mnt/server' --recursion --add-file={mods/,config/,scripts/,libraries/,structures/,resources/,plugins/,startup.sh} -czpf "backup-$(date -u -Is | sed -e 's/\+00\:00/Z/' -e 's/\:/-/g').tar.gz"
mv ./backup*.tar.gz /mnt/server/
echo "Backup completed!"
#Now, on to the stuff you came here for
cd /mnt/server

# Remove old modpack files
rm -rf mods/
rm -rf config/
rm -rf scripts/
rm -rf libraries/
rm -rf structures/
rm -rf resources/
rm -rf plugins/
rm -rf startup.sh
rm "$SERVER_JARFILE"

echo "Removed old modpack files"

URL=$(curl -s "https://addons-ecs.forgesvc.net/api/v2/addon/$PROJECT_ID/file/$FILE_ID/download-url")


# Download modpack server zip and extract to temporary directory.
curl -s "$URL" -L --output modpack.zip
mkdir -p install
unzip modpack.zip -d install
rm modpack.zip
echo "Extracted fresh installation files"

# Copy desired folders from temporary directory
if [ -d "install/mods" ]; then
    cp -R install/mods /mnt/server/
    cp -R install/config /mnt/server/
    cp -R install/scripts /mnt/server/
    cp -R install/structures /mnt/server/
else
    cp -R install/*/mods /mnt/server/
    cp -R install/*/config /mnt/server/
    cp -R install/*/scripts /mnt/server/
    cp -R install/*/structures /mnt/server/
fi
echo "Moved over the necessary modpack files from fresh installation"

# Remove certain mods
rm mods/*LagGoggles*
rm mods/*AromaBackup*
rm mods/*BuildingGadgets*
rm mods/*champions*
rm mods/*ChanceChubes*
rm mods/*craftingtweaks*
rm mods/*DimensionalDoors*
rm mods/*FastWorkbench*
rm mods/*FTBUtilities*
rm mods/*Explosives*
rm mods/*ICBM*
rm mods/*InventoryTweaks*
rm mods/*minecolonies*
rm mods/*MouseTweaks*
rm mods/*Phosphor*
rm mods/*PortalGun*
rm mods/*rftoolsdim*
rm mods/*signpost*

curl -s "https://gitlab.com/api/v4/projects/19660209/jobs/artifacts/master/raw/sponge.tar.gz?job=build" -L --output sponge.tar.gz
tar -C install/ -xvf sponge.tar.gz
rm sponge.tar.gz


cp -R install/sponge/modpack-specific/"$MODPACK_TAG"/* install/sponge
rm -rf install/sponge/modpack-specific
cp -R install/sponge/* /mnt/server/
chmod +x startup.sh

#Config overrides via sed go here
echo "Setting modpack tag for core to $MODPACK_TAG"
sed -e "s/\"modpack-tag\"\:.*/\"modpack-tag\"\: \"$MODPACK_TAG\"/" -i /mnt/server/pluginconfig/modrealmscore/core.json
echo "Setting server for luckperms to $MODPACK_TAG"
sed -e "s/server\=.*/server=\"$MODPACK_TAG\"/" -i /mnt/server/pluginconfig/luckperms/luckperms.conf

mv forge/forge*.jar forge/"$SERVER_JARFILE"
cp -R forge/* /mnt/server/
rm -rf forge
echo "Downloaded and setup the modpack-specific files and the startup script."

rm -rf install
rm -rf install.sh
echo "Cleaned up!"
echo "<CHANGE> Installation Script Complete!";
