#!/bin/bash
# PerfTest Installation Script
# Arguments: $1=Project ID $2=File ID
echo "Performance Testing Script Started!";

# Setting Variables
PROJECT_ID=$1
echo -e "Project ID: $PROJECT_ID"
FILE_ID=$2
echo -e "File ID: $FILE_ID"
SERVER_JARFILE=$3
echo -e "Project ID: $SERVER_JARFILE"
CF=$4

#Before we do anything important, make a backup in gzip-compressed tape archive format
#Done from outside of the server directory to prevent tar from complaining about "." having changed as it was read
echo "Creating backup..."
cd /tmp
tar --restrict --directory='/mnt/server' --recursion --add-file={mods/,config/,scripts/,libraries/,structures/,resources/,plugins/,startup.sh} -czpf "backup-$(date -u -Is | sed -e 's/\+00\:00/Z/' -e 's/\:/-/g').tar.gz"
mv ./backup*.tar.gz /mnt/server/
echo "Backup completed!"
#Now, on to the stuff you came here for
cd /mnt/server

# Remove old modpack files
find . ! -name 'install.sh' -type d -exec rm -rf {} +

echo "Removed old files"
mkdir -p install

if [ "$CF" == "true" ]; then
    URL=$(curl -s "https://addons-ecs.forgesvc.net/api/v2/addon/$PROJECT_ID/file/$FILE_ID/download-url")


    # Download modpack server zip and extract to temporary directory.
    curl -s "$URL" -L --output modpack.zip

    unzip modpack.zip -d install
    rm modpack.zip
    echo "Extracted fresh installation files"

    # Copy desired folders from temporary directory 
    if [ -d "install/mods" ]; then
        cp -R install/mods /mnt/server/
        cp -R install/config /mnt/server/
        cp -R install/scripts /mnt/server/
        cp -R install/structures /mnt/server/
        cp -R install/libraries /mnt/server/
    else
        cp -R install/*/mods /mnt/server/
        cp -R install/*/config /mnt/server/
        cp -R install/*/scripts /mnt/server/
        cp -R install/*/structures /mnt/server/
        cp -R install/*/libraries /mnt/server/
    fi
    echo "Moved over the necessary modpack files from fresh installation"
fi

curl -s "https://gitlab.com/api/v4/projects/19660209/jobs/artifacts/master/raw/perftest.tar.gz?job=build" -L --output perftest.tar.gz
tar -C install/ -xvf perftest.tar.gz
rm perftest.tar.gz

cp -R install/perftest/* /mnt/server/
chmod +x startup.sh

echo "Downloaded and setup the files and the startup script."

mv forge*-universal.jar $SERVER_JARFILE

rm -rf install
rm -rf install.sh
echo "Cleaned up!"
echo "Performance Testing Installation Script Complete!";