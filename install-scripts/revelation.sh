#!/bin/bash
# FTB Revelation Installation Script
# Arguments: $1=Project ID $2=File ID $3=Server Jarfile $4=Modpack Tag
echo "FTB Revelation Installation Script Started!";

# Setting Variables
PACK_ID=$1
echo -e "Pack ID: $PACK_ID"
VERSION_ID=$2
echo -e "Version ID: $VERSION_ID"
SERVER_JARFILE=$3
echo -e "Server Jarfile: $SERVER_JARFILE"
MODPACK_TAG=$4
echo -e "Modpack Tag: $MODPACK_TAG"

#Before we do anything important, make a backup in gzip-compressed tape archive format
#Done from outside of the server directory to prevent tar from complaining about "." having changed as it was read
echo "Creating backup..."
cd /tmp
tar --restrict --directory='/mnt/server' --recursion --add-file={mods/,config/,scripts/,libraries/,structures/,resources/,plugins/,startup.sh} -czpf "backup-$(date -u -Is | sed -e 's/\+00\:00/Z/' -e 's/\:/-/g').tar.gz"
mv ./backup*.tar.gz /mnt/server/
echo "Backup completed!"
#Now, on to the stuff you came here for
cd /mnt/server

# Remove old modpack files
rm -rf install/
rm -rf mods/
rm -rf config/
rm -rf scripts/
rm -rf libraries/
rm -rf structures/
rm -rf resources/
rm -rf plugins/
rm -rf startup.sh
rm "$SERVER_JARFILE"
echo "Removed old modpack files"

mkdir install

echo -e "Download modpack server zip and extract to temporary directory."

curl -sSL "https://api.modpacks.ch/public/modpack/$PACK_ID/$VERSION_ID/server/linux" -o packinstall

if [[ -f "./packinstall" ]]; then
    echo -e "packinstall has been downloaded and exists!"
else
    echo -e "packinstall doesn't exist!"
fi

chmod +x ./packinstall && echo -e "Added executable permissions to packinstall"
echo -e "Now running packinstall"
./packinstall "$PACK_ID" "$VERSION_ID" --path ./install --auto

rm ./packinstall
echo "Extracted fresh installation files"

# Copy desired folders from temporary directory 
if [ -d "install/mods" ]; then
    cp -R install/mods /mnt/server/
    cp -R install/config /mnt/server/
    cp -R install/scripts /mnt/server/

    # Forge Installation
    cp -R install/libraries /mnt/server/
    cp -R install/minecraft_server*.jar /mnt/server/
    cp -R install/forge-*-universal.jar /mnt/server/"$SERVER_JARFILE"
fi

echo "Moved over the necessary modpack files from fresh installation"

curl -s "https://gitlab.com/api/v4/projects/19660209/jobs/artifacts/master/raw/sponge.tar.gz?job=build" -L --output sponge.tar.gz
tar -C install/ -xvf sponge.tar.gz
rm sponge.tar.gz

# Remove certain mods
rm mods/*LagGoggles*
rm mods/*AromaBackup*
rm mods/*BuildingGadgets*
rm mods/*champions*
rm mods/*ChanceCubes*
rm mods/*craftingtweaks*
rm mods/*DimensionalDoors*
rm mods/*FTBUtilities*
rm mods/*Explosives*
rm mods/*ICBM*
rm mods/*InventoryTweaks*
rm mods/*minecolonies*
rm mods/*MouseTweaks*
rm mods/*phosphor*
rm mods/*PortalGun*
rm mods/*rftoolsdim*
rm mods/*signpost*


cp -R install/sponge/modpack-specific/"$MODPACK_TAG"/* install/sponge
rm -rf install/sponge/modpack-specific
cp -R install/sponge/* /mnt/server/
chmod +x startup.sh

#Config overrides brought to you by GNU sed
echo "Setting modpack tag for core to $MODPACK_TAG"
sed -e "s/\"modpack-tag\"\:.*/\"modpack-tag\"\: \"$MODPACK_TAG\"/" -i /mnt/server/pluginconfig/modrealmscore/core.json
echo "Setting server for luckperms to $MODPACK_TAG"
sed -e "s/server\=.*/server=\"$MODPACK_TAG\"/" -i /mnt/server/pluginconfig/luckperms/luckperms.conf
if [ -f /mnt/server/config/LagGoggles-server.cfg ]; then
    sed -f /mnt/server/sed/LagGoggles-server.sed -i /mnt/server/config/LagGoggles-server.cfg
else
    echo -e "# Configuration file\\n\\ngeneral {\\n\\t# Allow normal users to see event subscribers?\\n\\tB:ALLOW_NON_OPS_TO_SEE_EVENT_SUBSCRIBERS=true\\n\\n\\t# What is the maximum HORIZONTAL range in blocks normal users can get results for?\\n\\tD:NON_OPS_MAX_HORIZONTAL_RANGE=999999\\n\\n\\t# If normal users can start the profiler, what is the maximum time in seconds?\\n\\tI:NON_OPS_MAX_PROFILE_TIME=120\\n\\n\\t# What is the maximum VERTICAL range in blocks normal users can get results for?\\n\\tD:NON_OPS_MAX_VERTICAL_RANGE=255\\n\\n\\t# If normal users can start the profiler, what is the cool-down between requests in seconds?\\n\\tI:NON_OPS_PROFILE_COOL_DOWN_SECONDS=0\\n\\n\\t# How often can normal users request the latest scan result in seconds?\\n\\tI:NON_OPS_REQUEST_LAST_SCAN_DATA_TIMEOUT=30\\n\\n\\t# From where should we range-limit blocks vertically for normal users?\\n\\t# This will override the MAX_VERTICAL_RANGE when the block is above this Y level\\n\\tI:NON_OPS_WHITELIST_HEIGHT_ABOVE=255\\n\\n\\t# What's the permission level available to non-operators (Normal players)?\\n\\t# Please note that this ONLY works on dedicated servers. If you\'re playing singleplayer or LAN, the FULL permission is used.\\n\\t# Available permissions in ascending order are:\\n\\t#\\t'NONE'  No permissions are granted, all functionality is denied.\\n\\t#\\t'GET'   Allow getting the latest scan result, this will be stripped down to the player's surroundings\\n\\t#\\t'START' Allow starting the profiler\\n\\t#\\t'FULL'  All permissions are granted, teleporting to entities, blocks\\n\\t# Valid values:\\n\\t# NONE\\n\\t# GET\\n\\t# START\\n\\t# FULL\\n\\tS:NON_OP_PERMISSION_LEVEL=FULL\\n}\\n\\n" > /mnt/server/config/LagGoggles-server.cfg
fi

echo "Downloaded and setup the modpack-specific files and the startup script."

rm -rf install
rm -rf install.sh
echo "Cleaned up!"
echo "FTB Revelation Installation Script Complete!";
