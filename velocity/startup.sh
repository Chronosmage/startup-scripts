PLUGIN_BRANCH=$1
HOST=$2
JARFILE=$3

mkdir -p plugins
curl -s "https://gitlab.com/api/v4/projects/14547790/jobs/artifacts/${PLUGIN_BRANCH}/raw/Velocity/build/libs/punishments-velocity.jar?job=build" -L --output plugins/punishments-velocity.jar

curl -s "https://gitlab.com/api/v4/projects/14547656/jobs/artifacts/${PLUGIN_BRANCH}/raw/Velocity/build/libs/chat-velocity.jar?job=build" -L --output plugins/chat-velocity.jar

curl -s "https://gitlab.com/api/v4/projects/13686960/jobs/artifacts/${PLUGIN_BRANCH}/raw/Velocity/build/libs/bridge-velocity.jar?job=build" -L --output plugins/bridge-velocity.jar

curl -s "https://gitlab.com/api/v4/projects/14547906/jobs/artifacts/${PLUGIN_BRANCH}/raw/Velocity/build/libs/rewards-velocity.jar?job=build" -L --output plugins/rewards-velocity.jar

curl -s "https://gitlab.com/api/v4/projects/14547834/jobs/artifacts/${PLUGIN_BRANCH}/raw/Velocity/build/libs/announcements-velocity.jar?job=build" -L --output plugins/announcements-velocity.jar

curl -s "https://gitlab.com/api/v4/projects/14548469/jobs/artifacts/${PLUGIN_BRANCH}/raw/Velocity/build/libs/verification-velocity.jar?job=build" -L --output plugins/verification-velocity.jar

curl -s "https://gitlab.com/api/v4/projects/14547943/jobs/artifacts/${PLUGIN_BRANCH}/raw/Velocity/build/libs/tickets-velocity.jar?job=build" -L --output plugins/tickets-velocity.jar

java -XX:+UseG1GC -Xmx2048M -Xms2048M -Dsun.rmi.dgc.server.gcInterval=2147483646 -XX:+UnlockExperimentalVMOptions -XX:G1NewSizePercent=20 -XX:G1ReservePercent=20 -XX:MaxGCPauseMillis=50 -XX:G1HeapRegionSize=32M -Dhost=${HOST} -jar ${JARFILE}